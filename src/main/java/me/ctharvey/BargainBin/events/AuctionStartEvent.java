package me.ctharvey.BargainBin.events;

import me.ctharvey.BargainBin.AuctionController;
import me.ctharvey.BargainBin.AuctionData.Auction;
import me.ctharvey.BargainBin.BargainBin;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class AuctionStartEvent extends Event {

    private Auction auction;
    private static final HandlerList handlers = new HandlerList();

    public AuctionStartEvent(Auction auction) {
        this.auction = auction;
    }

    public Auction getAuction() {
        return this.auction;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public void tryToStart(Player player) {
        String prefix = AuctionController.getPrefix();
        if (AuctionController.getAuction() != null) {
            player.sendMessage(prefix + "There is already an auction in progress.");
        } else if (!BargainBin.econHandler.has(player.getName(), 10)) {
            player.sendMessage(prefix + "You can't afford the 10$ fee to start an auction.");
        } else if (isBlockedItem(auction.getItemStack())) {
            player.sendMessage(prefix + "You have not met the minimum requirement for this item which is " + AuctionController.getBlockedItems().get(auction.getItemStack().getType()));
        } else {
            startNewAuction(player);

        }
    }

    private boolean isBlockedItem(ItemStack itemStack) {
        if (AuctionController.getBlockedItems().containsKey(itemStack.getType())) {
            int minAmount = AuctionController.getBlockedItems().get(itemStack.getType());
            if (itemStack.getAmount() >= minAmount) {
                return false;
            }
            return true;
        }
        return false;
    }

    private void startNewAuction(Player player) {
        auction.setTime(60);
        AuctionController.setAuction(auction);
        player.getInventory().clear(player.getInventory().getHeldItemSlot());
        BargainBin.newSend(player, AuctionController.getPrefix(), "You are charged 10$ flat fee for this auction.");
        BargainBin.econHandler.withdrawPlayer(player.getName(), 10);
        AuctionController.broadcastAuctionInfo("&C" + auction.getAuctionStarter().getName() + " has started an auction!");
        AuctionController.broadcastAuctionInfo(auction.getAuctionInfo());
        AuctionController.initNewNote();
        
    }
}