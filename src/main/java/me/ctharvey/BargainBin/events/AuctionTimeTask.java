package me.ctharvey.BargainBin.events;

import me.ctharvey.BargainBin.AuctionController;
import me.ctharvey.BargainBin.BargainBin;

public class AuctionTimeTask implements Runnable {

    private BargainBin plugin;

    public AuctionTimeTask(BargainBin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        AuctionTimeEvent event = new AuctionTimeEvent(AuctionController.getAuction());
        plugin.getServer().getPluginManager().callEvent(event);
    }
}
