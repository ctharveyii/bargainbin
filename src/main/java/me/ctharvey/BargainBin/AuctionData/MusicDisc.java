/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.BargainBin.AuctionData;

import org.bukkit.Material;

/**
 *
 * @author thronecth
 */
public class MusicDisc {

    private Material type;
    private MusicDiscType mdType;

    public static enum MusicDiscType {

        GOLD_RECORD("13"),
        GREEN_RECORD("cat"),
        RECORD_3("blocks"),
        RECORD_4("chirp"),
        RECORD_5("far"),
        RECORD_6("mall"),
        RECORD_7("mellohi"),
        RECORD_8("stal"),
        RECORD_9("strad"),
        RECORD_10("ward"),
        RECORD_11("11"),
        RECORD_12("wait");

        private final String name;

        MusicDiscType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

    }

    public MusicDisc(Material type) {
        this.type = type;
        mdType = MusicDiscType.valueOf(type.toString());
    }

    public Material getType() {
        return type;
    }

    public MusicDiscType getMdType() {
        return this.mdType;
    }

    public String getName() {
        return "Record called " + getMdType().getName();
    }

}
