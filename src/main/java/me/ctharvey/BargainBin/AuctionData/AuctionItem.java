/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.BargainBin.AuctionData;

import java.util.List;
import java.util.Map;
import me.ctharvey.BargainBin.BargainBin;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.material.LongGrass;
import org.bukkit.material.Sandstone;
import org.bukkit.material.SmoothBrick;
import org.bukkit.material.Step;
import org.bukkit.material.Tree;
import org.bukkit.material.WoodenStep;
import org.bukkit.material.Wool;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

/**
 *
 * @author charles
 */
public final class AuctionItem {

    private final ItemStack item;
    private int ItemTypeID;
    private final Material type;
    private final BaseItemType baseType;
    private boolean hasLore = false;
    private boolean hasCustomName = false;
    private boolean hasEnchants = false;
    private final String extraInfo;

    public AuctionItem(ItemStack item) {
        this.item = item;
        this.type = item.getType();
        this.baseType = BaseItemType.getBaseItemType(item);
        if (this.item.hasItemMeta()) {
            if (this.item.getItemMeta().hasLore()) {
                this.hasLore = true;
            }
            if (this.item.getItemMeta().hasDisplayName()) {
                this.hasCustomName = true;
            }
        }
        if (!this.item.getEnchantments().isEmpty()) {
            this.hasEnchants = true;
        }
        this.extraInfo = BaseItemType.getExtendInfo(this);
    }

    public String getItemName() {
        return StringUtils.capitalize(this.getItemName(true).toLowerCase());
    }

    private String getItemName(Boolean boo) {
        String name;
        switch (type) {
            case LOG:
                Tree log = (Tree) item.getData();
                if (log.getSpecies() == TreeSpecies.GENERIC) {
                    name = "Oak Log";
                } else {
                    name = log.getSpecies().toString().toUpperCase() + " Log";
                }
                return name;
            case WOOD:
                log = (Tree) item.getData();
                if (log.getSpecies() == TreeSpecies.GENERIC) {
                    name = "Oak Plank";
                } else if (log.getSpecies() == TreeSpecies.REDWOOD) {
                    name = "Dark Oak Plank";
                } else {
                    name = log.getSpecies().toString().toUpperCase() + " Plank";
                }
                return name;
            case STEP:
                Step step = (Step) item.getData();
                name = step.getMaterial().toString().toUpperCase() + " Slab";
                return name;
            case SMOOTH_BRICK:
                SmoothBrick brick = (SmoothBrick) item.getData();
                name = brick.getMaterial().toString().toUpperCase() + " Brick";
                return name;
            case DOUBLE_PLANT:
                switch (item.getData().getData()) {
                    case 0:
                        return "Sunflower";
                    case 1:
                        return "Lilac";
                    case 3:
                        return "Azure Bluet";
                    case 4:
                        return "Rose Bush";
                    case 5:
                        return "Peony";
                }
                break;
            case RED_ROSE:
                switch (item.getData().getData()) {
                    case 0:
                        return "Poppy";
                    case 1:
                        return "Blue Orchid";
                    case 2:
                        return "Allium";
                    case 3:
                        return "Azure Bluet";
                    case 4:
                        return "Red Tulip";
                    case 5:
                        return "Orange Tulip";
                    case 6:
                        return "White Tulip";
                    case 7:
                        return "Pink Tulip";
                    case 8:
                        return "Oxeye Daisy";
                }
                break;
            case POTION:
                if (item.getDurability() < 1) {
                    return "Water Bottle";
                }
                Potion pot = Potion.fromItemStack(item);
                pot.getEffects();
                name = "";
                if (pot.isSplash()) {
                    name = "Splash ";
                }
                name += pot.getEffects().iterator().next().getType().getName().toUpperCase() + " " + pot.getEffects().iterator().next().getType().getDurationModifier();
                return name;
            case STAINED_CLAY:
                DyeColor color = DyeColor.values()[item.getData().getData()];
                name = StringUtils.capitalize(color.toString()) + " Stained Clay";
                return name;
            case STAINED_GLASS:
                color = DyeColor.values()[item.getData().getData()];
                name = StringUtils.capitalize(color.toString()) + " Glass";
                return name;
            case STAINED_GLASS_PANE:
                color = DyeColor.values()[item.getData().getData()];
                name = StringUtils.capitalize(color.toString()) + " Glass Pane";
                return name;
            case SANDSTONE:
                Sandstone sandstone = (Sandstone) item.getData();
                name = sandstone.getType().toString() + " Sandstone";
                return name;
            case DIRT:
                switch (item.getData().getData()) {
                    case 0:
                        return "Dirt";
                    case 1:
                        return "Mycelium";
                    case 2:
                        return "Podzol";
                }
                break;
            case COAL:
                switch (item.getData().getData()) {
                    case 0:
                        return "Coal";
                    case 1:
                        return "Charcoal";
                }
                break;
            case ANVIL:
                switch (item.getData().getData()) {
                    case 0:
                        return "Pristine Anvil";
                    case 1:
                        return "Slightly Damaged Anvil";
                    case 2:
                        return "Very Damaged Anvil";
                }
                break;
            case COBBLE_WALL:
                switch (item.getData().getData()) {
                    case 0:
                        return "Cobble Wall";
                    case 1:
                        return "Mossy Cobble Wall";
                }
                break;
            case PISTON_STICKY_BASE:
                return "Sticky Piston";
            case PISTON_BASE:
                return "Piston";
            case RAW_FISH:
                switch (item.getData().getData()) {
                    case 0:
                        return "Raw Fish";
                    case 1:
                        return "Raw Salmon";
                    case 2:
                        return "Clownfish";
                    case 3:
                        return "Pufferfish";
                }
                break;
            case QUARTZ_BLOCK:
                switch (item.getData().getData()) {
                    case 0:
                        return "Quartz Block";
                    case 1:
                        return "Chiseled Quartz Block";
                    case 2:
                        return "Pillar Quartz Vertical";
                    case 3:
                        return "Pillar Quartz North-South";
                    case 4:
                        return "Pillar Quartz East-West";
                }
                break;
            case GOLDEN_APPLE:
                switch (item.getData().getData()) {
                    case 0:
                        return "Golden Apple";
                    case 1:
                        return "Enchanted Golden Apple";
                }
            case LONG_GRASS:
                LongGrass grass = (LongGrass) item.getData();
                return StringUtils.capitalize(grass.getSpecies().toString().toLowerCase());
            case SAND:
                switch (item.getData().getData()) {
                    case 0:
                        return "Sand";
                    case 1:
                        return "Red Sand";
                }
                break;
            case COOKED_FISH:
                switch (item.getData().getData()) {
                    case 0:
                        return "Cooked Fish";
                    case 1:
                        return "Cooked Salmon";
                }
                break;
            case WOOD_STEP:
                WoodenStep wStep = (WoodenStep) item.getData();
                name = wStep.getSpecies().toString().toUpperCase() + " Step";
                return name;
            case SAPLING:
                log = (Tree) item.getData();
                if (log.getSpecies() == TreeSpecies.GENERIC) {
                    name = "Oak Plank";
                } else if (log.getSpecies() == TreeSpecies.REDWOOD) {
                    name = "Dark Oak Plank";
                } else {
                    name = log.getSpecies().toString().toUpperCase() + " Sapling";
                }
                return name;

        }
        if (item.getType().toString().contains("RECORD")) {
            MusicDisc disc = new MusicDisc(item.getType());
            return disc.getName();
        }
        name = this.item.getType().toString().substring(0, 1).toUpperCase() + this.item.getType().toString().substring(1).toLowerCase().replaceAll("_", " ");
        return name;
    }

    public String getCustomName() {
        if (this.item.hasItemMeta()) {
            if (item.getItemMeta().getDisplayName() != null) {
                return ChatColor.AQUA + item.getItemMeta().getDisplayName() + ChatColor.RED;
            }
        }
        return null;
    }

    public ItemStack getItem() {
        return item;
    }

    public int getItemTypeID() {
        return ItemTypeID;
    }

    public Material getType() {
        return type;
    }

    public BaseItemType getBaseType() {
        return baseType;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public boolean hasLore() {
        return hasLore;
    }

    public boolean hasCustomName() {
        return hasCustomName;
    }

    public boolean hasEnchants() {
        return hasEnchants;
    }

    public List<String> getLore() {
        return getItem().getItemMeta().getLore();
    }

    public Map<Enchantment, Integer> getEnchantments() {
        if (type == Material.ENCHANTED_BOOK) {
            EnchantmentStorageMeta bookMeta = (EnchantmentStorageMeta) item.getItemMeta();
            return bookMeta.getStoredEnchants();
        }
        return getItem().getEnchantments();
    }

    public String getDurabiltyInfo() {
        int currentDurability = item.getType().getMaxDurability() - item.getDurability();
        int maxDurability = item.getType().getMaxDurability();
        ChatColor durabilityIndicator = getDurabilityColor(maxDurability, currentDurability);
        String durability = durabilityIndicator + "" + currentDurability + ChatColor.GRAY + "/" + ChatColor.WHITE + item.getType().getMaxDurability();
        return durability;
    }

    private ChatColor getDurabilityColor(int maxDurability, int currentDurability) {
        ChatColor durabilityIndicator;
        double percentageOfDurability = ((double) currentDurability / (double) item.getType().getMaxDurability()) * (double) 100;
        if (percentageOfDurability >= 75) {
            durabilityIndicator = ChatColor.GREEN;
        } else if (percentageOfDurability >= 50 && percentageOfDurability < 75) {
            durabilityIndicator = ChatColor.YELLOW;
        } else if (percentageOfDurability >= 25 && percentageOfDurability < 50) {
            durabilityIndicator = ChatColor.RED;
        } else {
            durabilityIndicator = ChatColor.DARK_RED;
        }
        return durabilityIndicator;
    }
}
