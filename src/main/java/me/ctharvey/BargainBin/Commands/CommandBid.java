package me.ctharvey.BargainBin.Commands;

import me.ctharvey.BargainBin.AuctionData.Auction;
import me.ctharvey.BargainBin.AuctionController;
import me.ctharvey.BargainBin.AuctionData.Bid;
import me.ctharvey.BargainBin.BargainBin;
import me.ctharvey.BargainBin.events.AuctionBidEvent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class CommandBid implements CommandExecutor {

    private BargainBin plugin;
    private final String prefix;

    public CommandBid(BargainBin plugin) {
        this.plugin = plugin;
        this.prefix = AuctionController.getPrefix();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof ConsoleCommandSender) {
            BargainBin.newSend(sender, AuctionController.getPrefix(), "Console cannot bid on items.");
        }
        Player player = (Player) sender;
        switch (args.length) {
            case 0:
                player.sendMessage(prefix + "Incorrect usage! Type /bid [amount]");
                return true;
            case 1:
                if (AuctionController.getAuction() != null) {
                    int amountBid = getAmount(args[0]);
                    Auction auction = AuctionController.getAuction();
                    Bid bid = new Bid(auction, player, amountBid);
                    AuctionBidEvent event = new AuctionBidEvent(bid);
                    plugin.getServer().getPluginManager().callEvent(event);
                } else {
                    player.sendMessage(prefix + "No current auctions.");
                }
                return true;
        }
        return false;
    }

    private int getAmount(String amt) {
        int amount = 0;
        try {
            amount = Integer.valueOf(amt);
        } catch (NumberFormatException e) {
        }
        return amount;
    }
}
