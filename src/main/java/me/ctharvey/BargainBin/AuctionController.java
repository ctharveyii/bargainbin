package me.ctharvey.BargainBin;

import com.adamantdreamer.foundation.ui.noteboard.Note;
import com.adamantdreamer.foundation.ui.noteboard.Unique;
import com.adamantdreamer.foundation.ui.target.Target;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import me.ctharvey.BargainBin.AuctionData.Auction;
import me.ctharvey.BargainBin.AuctionData.AuctionItem;
import me.ctharvey.BargainBin.AuctionData.BaseItemType;
import me.ctharvey.BargainBin.AuctionData.Bid;
import me.ctharvey.BargainBin.AuctionData.EnchantmentNames;
import me.ctharvey.BargainBin.AuctionData.Numerals;
import me.ctharvey.BargainBin.events.AuctionBidEvent;
import me.ctharvey.BargainBin.events.AuctionEndEvent;
import me.ctharvey.BargainBin.events.AuctionStartEvent;
import me.ctharvey.BargainBin.events.AuctionTimeEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class AuctionController implements Listener {

    private static BargainBin plugin;
    private static final String prefix = "[" + ChatColor.BLUE + "BB" + ChatColor.RESET + "] ";
    private static Note note;
    private static Auction auction = null;
    private static BargainTarget playersWithNotifOff = new BargainTarget();
    private static Map<Material, Integer> blockedItems = new HashMap<>();

    public static Auction getAuction() {
        return auction;
    }

    public static void broadcastAuctionInfo(String... strings) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (playersWithNotifOff.contains(player)) {
                for (String line : strings) {
                    BargainBin.newSend(player, prefix, line);
                }
            }
        }
    }

    public static Auction setAuction(Auction auction) {
        AuctionController.auction = auction;
        return AuctionController.auction;
    }

    public static void initNewNote() {
        note = new Note("BargainBin", unique, Note.Priority.HIGH, 0);
        setBoardText(note);
        note.show(playersWithNotifOff);
    }

    private static String getItemDetails() {
        AuctionItem item = auction.getItem();
        String itemDetails = "";

        if (item.getItem().getAmount() > 1) {
            itemDetails = ChatColor.RED + "Items: " + ChatColor.AQUA + auction.getItem().getItem().getAmount() + " " + auction.getItem().getItemName();
        } else {
            itemDetails = ChatColor.RED + "Item: " + ChatColor.AQUA + auction.getItem().getItemName();
        }
        if (itemDetails.length() > 16) {
            if (itemDetails.length() > 32) {
                itemDetails = itemDetails.substring(0, 32);
            }
            int cutPoint = itemDetails.length() - 16;
            itemDetails = itemDetails.substring(0, cutPoint)
                    + itemDetails.substring(cutPoint, itemDetails.length());
        }
        return itemDetails;
    }

    private static float minimumBidPercent = 1;
    private static float auctionFeePercent = 2;

    public AuctionController(BargainBin plugin) {
        AuctionController.plugin = plugin;
    }

    public static BargainTarget getPlayersWithNotifOff() {
        return playersWithNotifOff;
    }

    public static Note getBoard() {
        return note;
    }

    public static String getPrefix() {
        return prefix;
    }

    public static BargainBin getPlugin() {
        return plugin;
    }

    public static Auction getTheauction() {
        return auction;
    }

    public String getEnchantmentName(Enchantment enchantment) {
        return EnchantmentNames.getName(enchantment.getName());
    }

    public static void disable() {
        if (auction != null) {
            Player player = auction.getAuctionStarter();
            ItemStack item = auction.getItem().getItem();
            processItemTransfer(player, auction.getItem());
            auction = null;
            try {
                note.close();
            } catch (ConcurrentModificationException ex) {
                note.setDuration(0);
                note.hide(playersWithNotifOff);
            }
            note = null;
        }
    }
    private static Unique unique = new Unique(true);

    @EventHandler
    public void onAuctionStart(AuctionStartEvent event) {
        Auction auction = event.getAuction();
        Player player = auction.getAuctionStarter();
        event.tryToStart(player);

    }

    @EventHandler
    public void onAuctionBid(AuctionBidEvent event) {
        Bid bid = event.getBid();
        Auction aAuction = bid.getAuction();
        Player player = bid.getPlayer();
        int amount = bid.getAmount();
        if (player.getName().equals(aAuction.getAuctionStarter().getName())) {
            player.sendMessage(prefix + "You can't bid on your own auction!");
            return;
        }
        if (BargainBin.econHandler.has(player.getName(), amount)) {
            float minimumBid = getMinimumBid(aAuction.getAmount(), aAuction.getBidder());
            if (minimumBid < aAuction.getAmount()) {
                minimumBid = aAuction.getAmount() + 10;
            }
            if (amount >= minimumBid) {
                aAuction.setWasBidOn(true);
                aAuction.setAmount(amount);
                aAuction.setBidder(player);
                if (aAuction.getTime() <= 15) {
                    aAuction.setTime(15);
                }
                float minBid = getMinimumBid(amount, aAuction.getBidder());
                note.setVar("amount", "" + minBid);
                note.setVar("bidder", player.getName());
                broadcastAuctionInfo("&cBID: &b$" + bid.getAmount() + " &cby &6" + bid.getPlayer().getName());
            } else {
                BargainBin.newSend(player, prefix, "Your bid isn't high enough.  The minimum bid is $" + minimumBid);
            }
        } else {
            BargainBin.newSend(player, prefix, "You don't have enough money for this bid.");
        }
    }

    private static float getMinimumBid(float amount, Player player) {
        if (player == null) {
            return amount;
        }
        if (amount * (minimumBidPercent / (float) 100) > 10) {
            return (amount * (minimumBidPercent / (float) 100) + amount);
        }
        return 10 + amount;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onLogin(PlayerLoginEvent e) {
        playersWithNotifOff.add(e.getPlayer());
        Bukkit.getScheduler().runTaskLater(plugin, new AuctionJoinRunnable(e.getPlayer()), 5);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onLogout(PlayerQuitEvent e) {
        playersWithNotifOff.remove(e.getPlayer());
    }

    private class AuctionJoinRunnable implements Runnable {

        private Player player;

        public AuctionJoinRunnable(Player player) {
            this.player = player;
        }

        @Override
        public void run() {
            if (playersWithNotifOff.contains(player) && getAuction() != null) {
                try {
                    note.show(player);
                } catch (IllegalStateException ex) {
                }
            }
        }
    }

    @EventHandler
    public void onAuctionEnd(AuctionEndEvent event) {
        Auction auction = event.getAuction();
        Player seller = auction.getAuctionStarter();
        Player bidder = auction.getBidder();
        double fee = (auctionFeePercent / 100) * auction.getAmount();
        if (auctionBidValid(auction)) {
            if (!auction.wasBidOn()) {
                broadcastAuctionInfo("Nobody bid on " + auction.getItem().getItemName());
                if (seller != null) {
                    processItemTransfer(seller, auction.getItem());
                }
                auction = null;
            } else {
                if (bidder != null) {
                    broadcastAuctionInfo(bidder.getName() + " purchased a " + auction.getItem().getItemName() + " for $" + auction.getAmount());
                    processTransaction(auction.getPlayer(), bidder.getName(), auction.getAmount());
                    processItemTransfer(bidder, auction.getItem());
                } else {
                    broadcastAuctionInfo(bidder.getName() + " is not online and the item is forfiet.");
                }
                if (fee > 1) {
                    BargainBin.newSend(seller, prefix, "You are charged a " + auctionFeePercent + "% fee on all auctions for $" + fee);
                    BargainBin.econHandler.withdrawPlayer(seller.getName(), fee);
                }
                AuctionController.auction = null;
            }
        }
        AuctionController.auction = null;
        //set the board on the right to off.
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!getPlayersWithNotifOff().contains(player)) {
                note.hide(player);
            }
        }
        try {
            if (note != null) {
                note.close();
            }
        } catch (ConcurrentModificationException ex) {
            note.hide(playersWithNotifOff);
        }
        note = null;
    }

    @EventHandler
    public void onAuctionTime(AuctionTimeEvent event) {
        Auction aAuction = event.getAuction();
        if (aAuction != null) {
            aAuction.setTime(aAuction.getTime() - 1);
            try {
                note.setVar("time", "" + (auction.getTime() - 1) + "s");
            } catch (NoSuchElementException e) {
                initNewNote();
            }
            if (aAuction.getTime() == 0) {
                AuctionEndEvent aeevent = new AuctionEndEvent(aAuction);
                plugin.getServer().getPluginManager().callEvent(aeevent);
            }
        }

    }

    private boolean auctionBidValid(Auction auction) {
        if (auction.getBidder() != null && !auction.getBidder().isOnline()) {
            broadcastAuctionInfo(ChatColor.RED + "The bidder has logged out.  Auction cancelled.  Abuse will be punished.");
            return false;
        }
        if (auction.wasBidOn()) {
            if (auction.getBidder() == null || !auction.getBidder().isOnline()) {
                broadcastAuctionInfo(ChatColor.RED + "The bidder has logged out.  Auction cancelled.  Abuse will be punished.");
                AuctionController.disable();
                return false;
            }
            if (!BargainBin.econHandler.has(auction.getBidder().getName(), auction.getAmount())) {
                broadcastAuctionInfo(ChatColor.RED + "The bidder no longer has funds for the auction.  Abuse is noted.");
                return false;
            }
        }
        return true;
    }

    //TODO pass names not players.
    private void processTransaction(String seller, String bidder, int amount) {
        BargainBin.econHandler.depositPlayer(seller, amount);
        BargainBin.newSend(Bukkit.getPlayer(seller), prefix, "&cYou have recieved &2$" + amount);
        BargainBin.econHandler.withdrawPlayer(bidder, amount);
        BargainBin.newSend(Bukkit.getPlayer(bidder), prefix, "&cYou have been debited &2$" + amount);
    }

    private static void processItemTransfer(Player bidder, AuctionItem item) {
        if (bidder != null && item != null) {
            bidder.sendMessage(prefix + "You have received " + item.getItem().getAmount() + " " + item.getItemName() + "(s)");
            if (bidder.getInventory().firstEmpty() == -1) {
                bidder.sendMessage(prefix + "Your inventory is full.  Item is at your feet.");
                bidder.getWorld().dropItemNaturally(bidder.getLocation(), item.getItem());
            } else {
                bidder.getInventory().addItem(item.getItem());
            }
        }
    }

//    public static List<String> getBoardText(Auction auction) {
//        List<String> lines = new ArrayList<>();
//        String itemDetails = ChatColor.RED + "" + auction.getItem().getItem().getAmount() + " * " + auction.getItem().getItemName();
//        if (itemDetails.length() > 16) {
//            if (itemDetails.length() > 32) {
//                itemDetails = itemDetails.substring(0, 32);
//            }
//            int cutPoint = itemDetails.length() - 16;
//            itemDetails = itemDetails.substring(0, cutPoint)
//                    + "|" + itemDetails.substring(cutPoint, itemDetails.length());
//        }
//        lines.add(itemDetails + "|:" + " $" + auction.getAmount());
//        String extraInfo = auction.getItem().getExtraInfo();
//        lines.add(ChatColor.RED + "ExtraInfo: " + ChatColor.WHITE + auction.getItem().getExtraInfo());
//        lines.add(ChatColor.RED + "Time| Remaining: " + ChatColor.GREEN + "|" + (auction.getTime() - 1) + "s");
//        lines.add(ChatColor.RED + "TopBidder: " + ChatColor.AQUA + "|" + ChatColor.GRAY + "None");
//        lines.add(ChatColor.GOLD + "Enchantments: " + auction.getItem().getEnchantments().size());
//        if (auction.getItem().getBaseType() != null && auction.getItem().getBaseType().equals(BaseItemType.TOOL)) {
//            lines.add(ChatColor.GRAY + "Durability: " + ChatColor.WHITE + auction.getItem().getDurabiltyInfo());
//        }
//        if (auction.getItem().getLore() != null) {
//            lines.add(ChatColor.GOLD + "Lores: " + auction.getItem().getLore().size());
//        }
//        return lines;
//    }
    private static Note setBoardText(Note note) {
        String itemDetails = getItemDetails();
        note.add(itemDetails);
        String customName = "";
        if (auction.getItem().hasCustomName()) {
            if (auction.getItem().getCustomName().length() > 16) {
                customName = ChatColor.RED + "(" + ChatColor.AQUA + auction.getItem().getCustomName().substring(0, 15) + ChatColor.RED + ")";
            } else {
                customName = ChatColor.RED + "(" + ChatColor.AQUA + auction.getItem().getCustomName() + ChatColor.RED + ")";

            }
        }
        if (!customName.isEmpty() && !"".equals(customName)) {
            note.add(ChatColor.RED + "Custom Name: " + ChatColor.DARK_AQUA + customName);
        }
        note.addVar("amount", ChatColor.RED + "Minimum Bid: " + ChatColor.DARK_GREEN + "${" + auction.getAmount() + "}");
        note.addVar("time", ChatColor.RED + "Time Remaining: " + ChatColor.GREEN + "{" + (auction.getTime() - 1) + "s}");
        note.addVar("bidder", ChatColor.RED + "TopBidder: " + ChatColor.GRAY + "{None}");
        if (auction.getItem().getBaseType() != null && auction.getItem().getBaseType().equals(BaseItemType.TOOL)) {
            note.add(ChatColor.GRAY + "Durability: " + ChatColor.WHITE + auction.getItem().getDurabiltyInfo());
        }
        if (auction.getItem().getEnchantments().size() > 0) {
//            note.add(ChatColor.GOLD + "# Enchantments: " + auction.getItem().getEnchantments().size());
            note.add(ChatColor.GRAY + "Enchantments: ");
            note.add(parseEnchantments(auction.getItem().getEnchantments()).toArray(new String[0]));
        }
        String extraInfo = auction.getItem().getExtraInfo();
        if (extraInfo != null) {
            note.add(ChatColor.RED + "ExtraInfo: " + ChatColor.WHITE + auction.getItem().getExtraInfo());
        }

        if (auction.getItem().getLore() != null) {
            note.add(ChatColor.GOLD + "Lores: ");
            List<String> lore = auction.getItem().getLore();
            List<String> loreDisplay = new ArrayList();
            for (String one : lore) {
                loreDisplay.add("    " + one);
            }
            note.add(loreDisplay.toArray(new String[0]));
        }
        return note;
//
//        note.addVar("time", ChatColor.RED + "Time| Remaining: " + ChatColor.GREEN + "|" + (auction.getTime() - 1) + "s");
//        note.addVar("topbidder", ChatColor.RED + "TopBidder: " + ChatColor.AQUA + "|" + ChatColor.GRAY + "None");
//        note.addVar("enchants", ChatColor.GOLD + "Enchantments: " + auction.getItem().getEnchantments().size());
//        if (extraInfo != null) {
//            note.addVar("extraInfo", ChatColor.RED + "ExtraInfo: " + ChatColor.WHITE + auction.getItem().getExtraInfo());
//        }
//
//        if (auction.getItem().getBaseType() != null && auction.getItem().getBaseType().equals(BaseItemType.TOOL)) {
//            note.addVar("durability", ChatColor.GRAY + "Durability: " + ChatColor.WHITE + auction.getItem().getDurabiltyInfo());
//        }
//        if (auction.getItem().getLore() != null) {
//            note.addVar("lore", ChatColor.GOLD + "Lores: " + auction.getItem().getLore().size());
//        }
    }

    private static List<String> parseEnchantments(Map<Enchantment, Integer> enchantments) {
        String enchantmentstr = "";
        List<String> enchants = new ArrayList();
        for (Map.Entry<Enchantment, Integer> ent : enchantments.entrySet()) {
            String enchantName = EnchantmentNames.getName(ent.getKey().getName());
            enchantmentstr = EnchantmentNames.getName(enchantName).substring(0, 1).toUpperCase() + ent.getKey().getName().toString().substring(1).toLowerCase().replaceAll("_", " ") + " "
                    + Numerals.convertIntegerToRoman(ent.getValue());
            enchants.add("    " + enchantmentstr);
        }
        return enchants;
    }

    public static Map<Material, Integer> getBlockedItems() {
        return blockedItems;
    }

    public static class BargainTarget extends Target {

        Set<CommandSender> players = new HashSet();

        @Override
        public Set<CommandSender> get() {
            return players;
        }

        public void add(Player cs) {
            players.add(cs);
        }

        public void add(CommandSender cs) {
            players.add(cs);
        }

        public void remove(Player player) {
            players.remove(player);
        }

        public void remove(CommandSender player) {
            players.remove(player);
        }

        public boolean contains(Player player) {
            if (this.players.contains(player)) {
                return true;
            }
            return false;
        }

        public boolean contains(CommandSender player) {
            if (this.players.contains(player)) {
                return true;
            }
            return false;
        }
    }
}
