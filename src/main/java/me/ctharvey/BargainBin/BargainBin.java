package me.ctharvey.BargainBin;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.BargainBin.Commands.CommandBid;
import me.ctharvey.BargainBin.Commands.CommandAuction;
import me.ctharvey.BargainBin.events.AuctionTimeTask;
import net.milkbowl.vault.economy.Economy;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class BargainBin extends JavaPlugin {

    private static AuctionController controller;

    @Override
    public void onEnable() {
        log("Enabled");
        this.setupEcon();
        controller = new AuctionController(this);
        registerCommands();
        registerListeners();
        registerTasks();
        if (econHandler == null) {
            log("Unable to initilize EconomyHandler.  Shutting down.");
            Bukkit.getPluginManager().disablePlugin(this);
        }
        configureConfig();
    }

    @Override
    public void onDisable() {
        log("Disabled");
        AuctionController.disable();
    }

    private void registerCommands() {
        this.getCommand("auction").setExecutor(new CommandAuction(this));
        this.getCommand("bid").setExecutor(new CommandBid(this));
        this.getCommand("a").setExecutor(new CommandAuction(this));
    }

    private void registerListeners() {
        this.getServer().getPluginManager().registerEvents(controller, this);
    }

    private void registerTasks() {
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new AuctionTimeTask(this), 20, 20);
    }

    public static void log(Object message) {
        String prefix = AuctionController.getPrefix();
        Object msg = message;
        if (msg instanceof String) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + msg));
        } else if (msg instanceof Integer) {
            Bukkit.getConsoleSender().sendMessage(prefix + Integer.toString((Integer) msg));
        } else {
            msg = new ReflectionToStringBuilder(msg);
            Bukkit.getConsoleSender().sendMessage(prefix + msg);
        }
    }

    private void configureConfig() {
        FileConfiguration config = this.getConfig();
        this.saveDefaultConfig();
        HashMap<String, Object> blockedItems = (HashMap<String, Object>) config.getConfigurationSection("Blocked_Items").getValues(false);
        int id;
        for (Entry<String, Object> entry : blockedItems.entrySet()) {
            try {
                id = Integer.parseInt(entry.getKey());
            } catch (NumberFormatException ex) {
                log(entry.getKey() + " is not a valid item id.");
                return;
            }
            Material mat = Material.getMaterial(id);
            if (mat != null) {
                AuctionController.getBlockedItems().put(mat, (Integer) entry.getValue());
            } else {
                log("Invalid id" + entry.getValue());
            }
        }
    }

    public static void newSend(CommandSender cs, String prefix, Object text) {
        if (cs == null) {
            try {
                throw new Exception("Unable to send message to Null player");
            } catch (Exception ex) {
                Logger.getLogger(BargainBin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (text == null) {
            try {
                throw new Exception("Unable to send empty message from " + cs.getName());
            } catch (Exception ex) {
                Logger.getLogger(BargainBin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String msg;
        if (text instanceof String) {
            msg = (String) text;
        } else if (text instanceof Integer) {
            msg = (Integer.toString((Integer) text));
        } else {
            msg = text.toString();
        }
        text = ChatColor.translateAlternateColorCodes('&', msg);
        prefix = ChatColor.translateAlternateColorCodes('&', prefix);
        cs.sendMessage(ChatColor.WHITE + prefix + text);
    }

    public static Economy econHandler = null;

    private boolean setupEcon() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econHandler = rsp.getProvider();
        return econHandler != null;
    }
}
